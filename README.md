# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
There is a .cpp file by the name main.cpp. It contains the code for logical simulation of a combinational circuit,toogle count btw 2 test vectors and static time analysis of the same. The development was done using Eclipse IDE.
It was also tested and debugged using g++ and gdb (independent of eclipse).
There are 2 files debug.cpp and debug.h. They are primarily used in debugging.
They have the debug trace enabler, where in one can enable/disable traces at various levels of criticaly.

* Configuration
Eclipse 3.8.1

* Dependencies
Platform to run and debug a c++ application.
Boost graph library

* Database configuration

* How to run tests
There are 4 files as input to the system. A hypergraph file, type file, rise time file and fall time file
* Deployment instructions


******************To be updated *******************************

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact