/*
 * debug.hpp
 *
 *  Created on: 18-Jun-2014
 *      Author: nslab1
 */

#ifndef DEBUG_HPP_
#define DEBUG_HPP_

class CLog
{
public:
    enum { All=0, Dfs, Debug, Info, Fatal, File,Error };
    static void Write(int nLevel, const char *szFormat, ...);
    static void SetLevel(int nLevel);

protected:
    static void CheckInit();
    static void Init();

private:
    CLog();
    static bool m_bInitialised;
    static int  m_nLevel;
};





#endif /* DEBUG_HPP_ */
