/*
 * main.c
 *
 *  Created on: 27-May-2014
 *      Author: nslab1
 */

#include "debug.hpp"
#include<time.h>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <iostream>
#include<list>
#include<stdio.h>
#include<fstream>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <map>
#include <boost/graph/properties.hpp>
#include<sstream>
#include <string>
#include<ctime>
#include <string.h>
#include<boost/tokenizer.hpp>
#include<boost/graph/depth_first_search.hpp>
#include <time.h>

using namespace std;
/*
 * Global variable definition
 */
int level;
unsigned int max_level;
unsigned int num_vertices;
float reqTime = 10.0;
vector<pair<int, int> > arr;
vector<pair<int, int> > eg;
vector<unsigned> list_pi;
vector<unsigned> list_po;
unsigned int toggle=0;
enum {
	AND, OR, NAND, NOR, NOT, EXOR, EXNOR, AND_OR_IN, OR_AND_IN, MUX,BU, PI, PO
};

struct VertexProperty {
	unsigned int level;
	bool output;
	bool output1;
	unsigned cycle;
	unsigned type;
	float delay;
	float req_time;
	float arr_time;
	float slack;
	int id;
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS,
		VertexProperty> Graph;

typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef typename boost::graph_traits<Graph> GraphTraits;

/*
 * Utility functions
 */

void staticTA(Graph& g);
bool evaluator(vector<bool>, int,unsigned int);
void execute(Graph &, int);
void replaceFlipFlop(Graph&, int);

bool compare(const pair<int, int>&i, const pair<int, int>&j) {
	return i.second < j.second;
}

void staticTA(Graph& g);

/*
 * cycle detector class
 * implicitly marks the initial levels to each node
 */

struct cycle_detector: public boost::dfs_visitor<> {
	cycle_detector(bool& has_cycle) :
			_has_cycle(has_cycle) {
		x = 0;
		y = 0;
	}

	template<typename Edge, typename Graph>
	void back_edge(Edge u, Graph & g) {

		x = u.m_source;
		y = u.m_target;
		_has_cycle = true;
		CLog::Write(CLog::Dfs, "Backedge", u, "\n");

		eg.push_back(make_pair(x, y));
	}

	template<typename Vertex, typename Graph>
	void discover_vertex(Vertex p, const Graph & g) const {

		CLog::Write(CLog::Dfs, "Discovered", p, "\n");
		arr.push_back(make_pair(p, level++));
	}

	template<typename Vertex, typename Graph>
	void finish_vertex(Vertex p, const Graph & g) const {

		CLog::Write(CLog::Dfs, "Finish", p, "\n");
		level = 1;
	}

protected:
	unsigned int x, y;
	bool& _has_cycle;

};

int main() {

	int lineCount = 1;
	string line;
	//vector<unsigned> list_pi;
//	vector<unsigned> list_po;
	vector<unsigned> list_ff;
	string xx;
	unsigned int Count = 1;
	ifstream tFile;
	ifstream in;
	ifstream ft;
	ifstream rt;
	ifstream pFile;
	unsigned num_nodes;
	bool has_cycle = false;
	unsigned int i = 0;

	clock_t init, final;
	init = clock();

	//in.open(		"/home/nslab1/Documents/research/pin-point/graphs/src/input.txt");
//in.open( "/home/nslab1/Documents/research/pin-point/parser/b14.hgr");

	in.open(
			"/home/nslab1/Documents/research/pin-point/graphs/src/benchmark_resources/hgr/b14.hgr");
	/*read from the hgr file the graph. First line has the num of vertices in the graph
	 */
	getline(in, line);
	if (!in) {
		CLog::Write(CLog::Error, "Error: Can't open the file named .hgr.\n");
		exit(1);
	}

	stringstream(line) >> num_vertices;

	num_nodes = num_vertices;
	num_vertices += 2;

	/*create a graph of size "num_vertices +2" nodes
	 */

	Graph g(num_vertices);

	/*
	 * initializing attributes to the first node
	 */
	g[0].output = false;
	g[0].output1 = false;
	g[0].cycle = -1;
	g[0].level = 0;
	g[0].type = 99;
	g[0].delay = 0.0;
	g[0].arr_time = 0.0;
	g[0].req_time = 0.0;
	g[0].slack = 0.0;
	g[0].id = 0;

	while (getline(in, line)) {
		string x;
		if (!line.empty()) {
			boost::tokenizer<> tok(line);
			int sub = 0;

			for (boost::tokenizer<>::iterator beg = tok.begin();
					beg != tok.end(); ++beg) {

				x = *beg;
				istringstream iss(x);
				iss >> sub;

				boost::add_edge(lineCount, sub + 1, g);

				/*CLog::Write(CLog::Info,  lineCount << "," << sub <<"\n");
				 *
				 */
			}
		}
		lineCount++;

		g[Count].delay = 0.0;
		g[lineCount].type = 99;

	}

	in.close();
	cout << lineCount << "\n";
	num_nodes = lineCount;

	line = "";
	//tFile.open("/home/nslab1/Documents/research/pin-point/graphs/src/type");

	//tFile.open( "/home/nslab1/Documents/research/pin-point/parser/b14.type");
	tFile.open(
			"/home/nslab1/Documents/research/pin-point/graphs/src/benchmark_resources/type/b14.type");
	if (!tFile) {
		CLog::Write(CLog::Error, "Error: Can't open the file named type.\n");
		exit(1);
	}

	/*
	 * read from type file and store it as a enum in 'type' field of graph object
	 */
	while (getline(tFile, xx)) {

		if (!xx.empty()) {
			line = xx.substr(0, 2);

			if (line.compare("AN") == 0) {

				g[Count].type = AND;

			} else if (line.compare("OR") == 0) {

				g[Count].type = OR;

			} else if (line.compare("ND") == 0) {

				g[Count].type = NAND;

			} else if (line.compare("NR") == 0) {

				g[Count].type = NOR;

			} else if (line.compare("IN") == 0) {

				g[Count].type = NOT;

			} else if (line.compare("XO") == 0) {

				g[Count].type = EXOR;
			} else if (line.compare("XN") == 0) {

				g[Count].type = EXNOR;
			} else if (line.compare("AO") == 0) {

				g[Count].type = AND_OR_IN;

			} else if (line.compare("OA") == 0) {

				g[Count].type = OR_AND_IN;

			} else if (line.compare("MX") == 0) {

				g[Count].type = MUX;

			} else if(line.compare("BU") == 0){
				g[Count].type = BU;

			} else if (line.compare("PI") == 0) {

				list_pi.push_back(Count);
				g[Count].type = PI;

			} else if (line.compare("PO") == 0) {

				list_po.push_back(Count);
				g[Count].type = PO;

			} else if (line.compare("FF") == 0) {

				list_ff.push_back(Count);
				list_pi.push_back(Count);
				//g[Count].type = 12;
				replaceFlipFlop(g, Count);
				g[Count].type = PI;

			}else
				g[Count].type = 99;

			/*initializing the attributes of remaining nodes
			 *
			 */
			g[Count].id = Count;
			g[Count].output = false;
			g[Count].output1 = false;
			g[Count].cycle = -1;
			g[Count].level = 0;
			g[Count].req_time = 0.0;
			g[Count].arr_time = 0.0;
			g[Count].slack = 0.0;
			g[Count].delay = 0.0;

			Count++;
		}
	}

	g[Count].id = Count;
	g[Count].output = false;
	g[Count].output1 = false;
	g[Count].cycle = -1;
	g[Count].level = 0;
	g[Count].delay = 0.0;
	g[Count].req_time = reqTime;
	g[Count].slack = 0.0;
	g[Count].type = 99;
	g[Count].arr_time = 0.0;

	tFile.close();

//	CLog::Write(CLog::Info,  num_vertices << "\n");

	/*identifying the PI and PO nodes and assigning the arrival times as 0.0
	 *
	 */

	pFile.open("/home/nslab1/Documents/research/pin-point/graphs/src/pin.txt");
	if (!pFile) {
		CLog::Write(CLog::Error, "Error: Can't open the file named pin.txt\n");
		exit(1);
	}
	  /* initialize random seed: */
			  srand (time(NULL));
	for (i = 0; i < list_pi.size(); i++) {

		boost::add_edge(0, list_pi[i], g);
		g[list_pi[i]].arr_time = 0.0;



		  /* generate secret number between 1 and 10: */
		 // Count = rand() % 10 + 1;

		//	if (Count %2 == 1)
		//	{
		g[list_pi[i]].output = true;
		//	}

		//	if (rand() %2 == 1)
		//	{
		g[list_pi[i]].output1 = false;
		//	}
	}




	for (i = 0; i < list_po.size(); i++) {
		boost::add_edge(list_po[i], Count, g);
		g[list_po[i]].req_time = reqTime;
	}

	lineCount = 1;

	/*
	 * read from type file and store it as a enum in 'type' field of graph object
	 */
	//rt.open("/home/nslab1/Documents/research/pin-point/graphs/src/rt");
	rt.open(
			"/home/nslab1/Documents/research/pin-point/graphs/src/benchmark_resources/rise/b14.rise");
	if (!rt) {
		CLog::Write(CLog::Error, "Error: Can't open the file named rise.\n");
		exit(1);
	}

	//ft.open("/home/nslab1/Documents/research/pin-point/graphs/src/ft");
	ft.open(
			"/home/nslab1/Documents/research/pin-point/graphs/src/benchmark_resources/fall/b14.fall");
	if (!ft) {
		CLog::Write(CLog::Error, "Error: Can't open the file named fall.\n");
		exit(1);
	}

	while (getline(ft, line) && (getline(rt, xx))) {
		string p, q;
		if (!line.empty()) {
			float sub1, sub2;

			p = line.substr(0, line.find("	"));
			q = xx.substr(0, xx.find("	"));

			istringstream iss(p);
			iss >> sub1;
			istringstream is(q);
			is >> sub2;

			g[lineCount].delay = (sub1 + sub2) / 2.0;
			//CLog::Write(CLog::Info,  sub1 << " " << sub2<< " " << g[lineCount].delay << "\n");
		}
		lineCount++;
	}
	ft.close();
	rt.close();

	final = clock() - init;

	/*
	 End of Reading from File
	 */

	typedef boost::graph_traits<Graph>::vertex_iterator vertex_iter;
	std::pair<vertex_iter, vertex_iter> vp;

	/*
	 identify the level for each vertex nodes
	 */

	cycle_detector vis(has_cycle);
	boost::depth_first_search(g, visitor(vis));

	/*copying level of each node to Graph after dfs
	 *
	 */

	for (i = 0; i < num_vertices; i++) {
		g[arr[i].first].level = arr[i].second;
		//	CLog::Write(CLog::Info,  arr[i].first << " -- " << arr[i].second << endl;
	}

	/*setting after analyzing dfs
	 *
	 */
	for (i = 0; i < eg.size(); i++) {
		g[eg[i].first].cycle = eg[i].second;
		//CLog::Write(CLog::Info,  eg[i].first << "," << eg[i].second << endl;
	}

	/*normalizing the levels
	 */
	typename GraphTraits::out_edge_iterator e_i, e_end;

	for (vp = vertices(g); vp.first != vp.second; ++vp.first) {
//CLog::Write(CLog::Info,  "\n");
		for (boost::tie(e_i, e_end) = boost::out_edges(*vp.first, g);
				e_i != e_end; ++e_i) {

			Vertex src = boost::source(*e_i, g);
			Vertex targ = boost::target(*e_i, g);
			//	CLog::Write(CLog::Info,  src << "-" << targ<<"\n");

			/* if out edge is part of the cycle ignore the edge, continue with next out-edge of the src
			 */
			if ((g[src].cycle == targ)) {
//							CLog::Write(CLog::Info,  src << "-" << targ<<"\n");
			} else {
				if (g[src].level >= g[targ].level) {
					g[targ].level = g[src].level + 1;

					//std::CLog::Write(CLog::Info,  "			(" << src << "," << targ << ") ";
				}
			}
		}
	}

	/*
	 for (vp = vertices(g); vp.first != vp.second; ++vp.first) {

	 for (boost::tie(e_i, e_end) = boost::out_edges(*vp.first, g);
	 e_i != e_end; ++e_i) {

	 Vertex src = boost::source(*e_i, g);
	 Vertex targ = boost::target(*e_i, g);
	 CLog::Write(CLog::Debug, "edge : %u - %u	", src, targ);
	 }
	 CLog::Write(CLog::Debug, "\n");
	 }
	 */

	for (i = 0; i < num_vertices; i++) {
		arr[i].second = g[arr[i].first].level;
		//	CLog::Write(CLog::Info,  i << " -- " << arr[i].first << "--" << arr[i].second << endl;
	}

	sort(arr.begin(), arr.end(), compare);
	//CLog::Write(CLog::Info,  "Level table";

	max_level = arr[num_vertices - 1].second;
	//CLog::Write(CLog::Info,  max_level << " \n ";

	/*carrying out level by level execution of nodes/gates
	 *
	 */
	for (i = list_pi.size()+1; i < num_vertices; i++) {
		CLog::Write(CLog::Info, "%u - %u\n", g[arr[i].first].id,
				g[arr[i].first].level);

		execute(g, arr[i].first);
	}

	staticTA(g);

	CLog::Write(CLog::Debug, "id		Type		level		delay		arr_time 	slack		output		output1\n");

	for (i = 1; i < num_vertices; i++) {

		CLog::Write(CLog::Debug, "%d		%u		%u		%.3f		%.3f		%.3f		%u		%u\n", g[i].id,
				g[i].type,g[i].level, g[i].delay, g[i].arr_time, g[i].slack, g[i].output, g[i].output1);
	}

	final=clock()-init;
	cout << (double) final / ((double) CLOCKS_PER_SEC)<<endl;
	cout << toggle;

	 fstream filestr;

	  filestr.open ("program3data.txt", fstream::in | fstream::out | fstream::app);

	  filestr<<"\n"; filestr<<num_nodes; filestr <<"	";
	  filestr<<(double) final / ((double) CLOCKS_PER_SEC);
	  filestr<<"		";
	  filestr<<toggle;

//ofstream dot("graph.dot");

	//write_graphviz(dot, g,
	//		boost::make_label_writer(boost::get(&VertexProperty::id, g)));

	return 0;
}

/*
 * given the required time, propagate the value backwards
 * and generate slack value for each node
 */

void staticTA(Graph& g) {

	typename GraphTraits::out_edge_iterator e_i, e_end;

	int i = 0;
	float min = 12.0;
	float min_rt = 12.0;

	g[num_vertices - 2].req_time = 12.0;

	/*iterate backwards from max_level nodes to PI's and calculate the required time and slack
	 *
	 */

	for (i = num_vertices - (list_po.size()+2); i > 0; i--) {
		min = 12.0;
		min_rt = 12.0;

		for (boost::tie(e_i, e_end) = boost::out_edges(arr[i].first, g);
				e_i != e_end; ++e_i) {

			Vertex src = boost::source(*e_i, g);
			Vertex targ = boost::target(*e_i, g);

			if (min > (g[targ].req_time - g[targ].delay - g[src].arr_time)) {

				min = g[targ].req_time - g[targ].delay - g[src].arr_time;
			}

			if (min_rt > (g[targ].req_time - g[targ].delay)) {

				min_rt = g[targ].req_time - g[targ].delay;
			}

			g[arr[i].first].slack = min;

			g[arr[i].first].req_time = min_rt;
		}

		CLog::Write(CLog::Info, "vertex %d l- %d", arr[i].first, arr[i].second);
		CLog::Write(CLog::Info, " req_time %f slack %f\n",
				g[arr[i].first].req_time, g[arr[i].first].slack);

	}
}

void execute(Graph& g, int vertex) {

	typename GraphTraits::in_edge_iterator in_i, in_end;
	vector<bool> inputs;
	vector<bool> inputs1;

	float max = 0.0;
	boost::tie(in_i, in_end) = boost::in_edges(vertex, g);

	CLog::Write(CLog::All, "inside execute vertex : ", vertex, "\n ");

	if (boost::in_degree(vertex, g) != 0) {
		for (; in_i != in_end; ++in_i) {

			/*identify the inputs for each gate and push there value to a input vector
			 */

			CLog::Write(CLog::All, "	src ", (*in_i).m_source, "\n");

			inputs.push_back(g[(*in_i).m_source].output);
			inputs1.push_back(g[(*in_i).m_source].output1);

			if ((g[(*in_i).m_source].type == PI
					|| g[(*in_i).m_source].type == PO
					|| g[(*in_i).m_source].id == 0)) {

			} else if (g[(*in_i).m_source].arr_time == 0.0) {

				CLog::Write(CLog::All, " ", __LINE__, "\n");
			}

			if (g[(*in_i).m_source].arr_time > max) {
				max = g[(*in_i).m_source].arr_time;
			}
		}
		g[vertex].arr_time = max + g[vertex].delay;
		/*
		*output attribute has first test vectors output.
		*output1 attribute has second test vectors ouput.
		*/
		g[vertex].output = evaluator(inputs, g[vertex].type,vertex);
		g[vertex].output1 = evaluator(inputs1, g[vertex].type,vertex);

		if(g[vertex].output != g[vertex].output1)
			toggle++;

		usleep(g[vertex].delay);

		CLog::Write(CLog::Info, "vertex %u arr_time %f output %u\n", vertex,
				g[vertex].arr_time, g[vertex].output);
	}
}

void replaceFlipFlop(Graph& g, int vertex) {

	/*
	 * If a flip flop is encountered, a set of actions are to be performed.
	 * 1.create a new node and make all the in-edges of flip flop to converge
	 * to this node.
	 * 2.Set the type of newly created node as PO
	 * 3.Set the type of flip flop node to PI
	 */

	typename GraphTraits::in_edge_iterator in_i, in_end;

	CLog::Write(CLog::All, "\n Inside replaceFlipFlop ", vertex, "\n");

	typename GraphTraits::edge_descriptor retrievedEdge;

	if (in_degree(vertex, g) != 0) {

		boost::tie(in_i, in_end) = boost::in_edges(vertex, g);

		for (; in_i != in_end; ++in_i) {

			typename GraphTraits::edge_descriptor retrievedEdge = boost::edge(
					(*in_i).m_source, (*in_i).m_target, g).first;

			g.remove_edge(retrievedEdge);

			if (boost::edge(retrievedEdge.m_source, retrievedEdge.m_target, g).second
					== false) {
				CLog::Write(CLog::Info, "Removed edge %u, %u\n",
						retrievedEdge.m_source, retrievedEdge.m_target);
			}

			boost::add_edge(retrievedEdge.m_source, num_vertices, g);

			if (boost::edge(retrievedEdge.m_source, num_vertices, g).second
					== true) {

				CLog::Write(CLog::Info, "\n Added Edge %u, %u\n",
						retrievedEdge.m_source, num_vertices, "\n");
			}

		}

		g[num_vertices].type = PO;
		g[num_vertices].id = num_vertices;
		g[num_vertices].output = false;
		g[num_vertices].output1 = false;
		g[num_vertices].cycle = -1;
		g[num_vertices].level = 0;
		g[num_vertices].delay = 0.0;
		g[num_vertices].req_time = reqTime;
		g[num_vertices].slack = 0.0;
		g[num_vertices].arr_time = 0.0;

		list_po.push_back(num_vertices);
	} else
	/*
	 * If there is no in_edges to the flip flop node ignore the creation of a new node
	 * But practically this is not possible, as a flip flop will always have a input pin
	 * As a extreme corner test case, this scenario is handled here
	 */

	{
		CLog::Write(CLog::Info, "Vertex doesn't have in_edges\n");

	}
	num_vertices += 1;
}

bool evaluator(vector<bool> inputs, int type, unsigned int vertex) {
	unsigned int iCount = 0;
	bool result;

	/*perform boolean operations for each node based on its type.
	 * For gates having more than one input, iterate over the 'inputs' vector
	 *  to get all the input values.
	 */

	CLog::Write(CLog::All, "\n Inside evaluator ", type, "\n");

	if (type == AND) {

		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++)
				result = inputs[iCount] & result;
		} else {
			cout<<vertex <<endl;

			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);
		}

	} else if (type == OR) {

		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++) {
				result = inputs[iCount] | result;
			}
		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);

		}

	} else if (type == NAND) {

		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++)
				result = inputs[iCount] & result;

			result = !(result);
		} else {
			cout<<vertex <<endl;
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);

		}

	} else if (type == NOR) {
		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++) {
				result = inputs[iCount] | result;
			}
			result = !(result);
		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);

		}

	} else if (type == NOT) {

		if (inputs.size() == 1) {

			result = !(inputs[0]);
			//	CLog::Write(CLog::Info,  inputs[0] << "-" << result << endl;

		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);
		}
	} else if (type == EXOR) {
		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++) {
				result = inputs[iCount] ^ result;
			}
		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);
		}
	} else if (type == EXNOR) {

		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++) {
				result = inputs[iCount] ^ result;
			}
			result = !(result);
		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);

		}
	} else if (type == AND_OR_IN) {

		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++) {
				result = inputs[iCount] ^ result;
			}
			result = !(result);
		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);

		}
	} else if (type == OR_AND_IN) {

		if (inputs.size() > 1) {
			result = inputs[0];

			for (iCount = 1; iCount < inputs.size(); iCount++) {
				result = inputs[iCount] ^ result;
			}
			result = !(result);
		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);

		}

	} else if (type == MUX) {

		if (inputs.size() > 1) {

			result = !(inputs[0]);
			//	CLog::Write(CLog::Info,  inputs[0] << "-" << result << endl;

		} else {
			CLog::Write(CLog::Error, "Invalid number of arguments :  %u\n",
					__LINE__);
		}

	} else if ((type < 0) || (type > 9)) {
		result = inputs[0];
	}
	return result;
}

